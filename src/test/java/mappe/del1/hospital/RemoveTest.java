package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A test class for the remove-method in Department
 * Consists of three internal classes that deal with positive and negative testing
 * Using try-catch in each test to handle the thrown Exceptions
 */

public class RemoveTest {

    /**
     * A class to test the remove-method for existing Person-objects
     * No exceptions should be thrown here
     * Positive testing
     */

    @Nested
    class RemoveExistingPersons {

        /**
         * A unit test to test if an existing patient is successfully removed from the patients list
         */
        @Test
        public void RemoveExistingPatient() {
            try {
                Department emergency = new Department("Akutten");
                Patient patient = new Patient("Ola", "Nordmann", "2039947871");
                emergency.addPatient(patient);

                assertTrue(emergency.getPatients().contains(patient));

                emergency.remove(patient);
            } catch (RemoveException | NullPointerException exception) {
                assertNull(exception);
            }
        }

        /**
         * A unit test to test if an existing employee is successfully removed from the employees list
         */

        @Test
        public void RemoveExistingEmployee() {
            try {
                Department emergency = new Department("Akutten");
                Employee employee = new Employee("Ingvild", "Snøfjell", "01019355687");
                emergency.addEmployee(employee);

                assertTrue(emergency.getEmployees().contains(employee));

                emergency.remove(employee);
            } catch (RemoveException | NullPointerException exception) {
                assertNull(exception);
            }
        }
    }

    /**
     * A class to test the remove-method for unexisting Person-objects
     * Expect RemoveException handling here
     * Negative testing
     */
    @Nested
    class RemoveUnexistingPersons {

        /**
         * A unit test that tests if the remove-method throws a RemoveException if removing an unexisting patient
         */

        @Test
        public void RemoveUnexistingPatient(){
            try{
                Department emergency = new Department("Akutten");
                Patient patient = new Patient("Ola", "Nordmann", "2039947871");

                assertFalse(emergency.getPatients().contains(patient));
                emergency.remove(patient);

            }
            catch (RemoveException | NullPointerException exception){
                assertEquals("Nordmann, Ola is not registered", exception.getMessage());
            }
        }

        /**
         *  A unit test that tests if the remove-method throws a RemoveException if removing an unexisting employee
         */

        @Test
        public void RemoveUnexistingEmloyee(){
            try{
                Department emergency = new Department("Akutten");
                Employee employee = new Employee("Ingvild", "Snøfjell", "01019355687");

                assertFalse(emergency.getEmployees().contains(employee));
                emergency.remove(employee);
            }
            catch(RemoveException | NullPointerException exception){
                assertEquals("Snøfjell, Ingvild is not registered", exception.getMessage());

            }
        }
    }

    @Nested
    class RemoveNull{
        /**
         * A class that test remove-method when a Person object equals null
         * Negative testing
         */

        @Test
        public void removeNullPerson(){
            try{
                Department emergency = new Department("Akutten");
                emergency.remove(null);

            }catch (RemoveException | NullPointerException exception){
                assertEquals("Person cannot be null", exception.getMessage());

            }
        }
    }
}