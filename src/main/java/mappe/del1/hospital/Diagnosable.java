package mappe.del1.hospital;

/**
 * An interface class that can set a diagnosis
 */

public interface Diagnosable {
    public abstract void setDiagnosis(String diagnosis);
}
