package mappe.del1.hospital;
import mappe.del1.hospital.exception.RemoveException;

import java.time.LocalDate;

import static javax.swing.JOptionPane.*;

public class HospitalClient {
    public static void main(String[] args) {

        Hospital hospital = new Hospital("St.Olavs");
        HospitalTestData.fillRegisterWithTestData(hospital);

        String [] muligheter = {"Fjern eksisterende ansatt", "Fjern ikke eksisterende pasient", "List alle ansatte", "List alle pasienter", "Avslutt"};
        final int REMOVE_EMPLOYEE = 0;
        final int REMOVE_UNEXISTING_PATIENT = 1; final int LIST_ALL_EMPLOYEES = 2; final int LIST_ALL_PATIENTS = 3; final int QUIT = 4;
        int valg = showOptionDialog(null, "Velg funksjon", "Mappeinnlevering del 1", YES_NO_OPTION,INFORMATION_MESSAGE, null, muligheter, muligheter[0]);

        while (valg != QUIT){
            switch (valg){
                case REMOVE_EMPLOYEE:
                    try{
                        Employee employee = new Employee("Odd Even", "Primtallet", "");

                        for(Department department: hospital.getDepartments()){
                            if(department.getDepartmentName().equals("Akutten")){
                                boolean successful = department.remove(employee);
                                if(successful){
                                    showMessageDialog(null, "Employee: " + employee.toString() + " is now removed as an employee in " + department.getDepartmentName());
                                }
                            }
                        }
                    } catch (RemoveException exception){
                        System.out.println(exception);
                    }
                    break;

                case REMOVE_UNEXISTING_PATIENT:
                    try{
                        Patient patient = new Patient("Elon", "Musk", "");

                        for(Department department: hospital.getDepartments()){
                            if(department.getDepartmentName().equals("Akutten")){
                                boolean sucessfull = department.remove(patient);
                                if(sucessfull){
                                    showMessageDialog(null,"Patient: " + patient.toString() + " is now removed as an patient in " + department.getDepartmentName());
                                }
                            }
                        }
                    } catch (RemoveException exception){
                        showMessageDialog(null, exception);
                    }
                    break;

                case LIST_ALL_EMPLOYEES:
                    for(Department department: hospital.getDepartments()){
                        showMessageDialog(null, department.getEmployees().toString());
                    }
                    break;

                case LIST_ALL_PATIENTS:
                    for(Department department: hospital.getDepartments()){
                        showMessageDialog(null, department.getPatients().toString());
                    }
                    break;

                default: break;
            }
            valg = showOptionDialog(null, "Velg funksjon", "Mappeinnlevering del 1", YES_NO_OPTION,INFORMATION_MESSAGE, null, muligheter, muligheter[0]);
        }










    }
}
