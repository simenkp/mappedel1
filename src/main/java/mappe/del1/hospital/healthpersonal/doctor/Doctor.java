package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;

/**
 * An abstract class that represents a doctor
 * inherits Employee
 */

public abstract class Doctor extends Employee {
   protected Doctor(String firstName, String lastName, String socialSecurityNumber){
       super(firstName, lastName, socialSecurityNumber);
   }

    /**
     * an abstract method that set a diagnosis on a spesific patient
     * @param p the patient that is being diagnosed
     * @param diagnosis
     */
   public abstract void setDiagnosis(Patient p, String diagnosis);
}
