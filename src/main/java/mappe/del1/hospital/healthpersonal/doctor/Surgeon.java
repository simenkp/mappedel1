package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 * A class that represents a surgeon
 * Inherits from the abstract class Doctor
 * A surgeon has the possibility to set a diagnosis
 */

public class Surgeon extends Doctor {
    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient p, String diagnosis){
        p.setDiagnosis(diagnosis);

    }
}
