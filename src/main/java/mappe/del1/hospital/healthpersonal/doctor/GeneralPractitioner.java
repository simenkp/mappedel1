package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 * A class that represents a general practitioner
 * Inherits from Doctor
 * A general practitioner has the possibility to set a diagnosis
 */

public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Overridet method inherited from Doctor
     * @param p the patient to be diagnosed
     * @param diagnosis the diagnosis
     */
    @Override
    public void setDiagnosis(Patient p, String diagnosis) {
        p.setDiagnosis(diagnosis);
    }
}
