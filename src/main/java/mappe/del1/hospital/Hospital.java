package mappe.del1.hospital;

import java.util.ArrayList;

/**
 * A class that represents a Hospital, which is a register of departments
 *
 * Using Arraylist since I need a dynamic, ordered list.
 * Hospital is an aggregate of Department. Although it would make more sense if it was a composite of Department,
 * as a hospital cannot live without departments.
 */

public class Hospital {
    private final String HOSPITALNAME;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName){
        this.HOSPITALNAME = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return HOSPITALNAME;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    } //Aggregation, not returning a copy

    /**
     * A method that add a department to departments-list
     * checks if department already exists before adding
     * Changed return value from "void" to "boolean", thus I can preserve the value at registration in client
     *
     * @param department the department that is being added
     * @return return true if the registration is successful, false if not
     */
    public boolean addDepartment(Department department){
        if(!departments.contains(department)){
            departments.add(department);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String res = "hospitalName: " + HOSPITALNAME + "\n";
        for (Department department : departments){
            res += department.toString() + "\n";
        }
        return res;
    }
}
