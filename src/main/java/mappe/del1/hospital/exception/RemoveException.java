package mappe.del1.hospital.exception;

public class RemoveException extends Throwable {
    private static final long serialVersionUID = 1L;
    private String exception;

    public RemoveException(String exception){
        super(exception);
    }


}
