package mappe.del1.hospital;

/**
 * An abstract class that represents a person
 * Is defined as abstract as it does not make sense to create objects of this class
 */

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * @param firstName persons first name
     * @param lastName persons last name
     * @param socialSecurityNumber persons social security number
     */

    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFullName(){
        return getLastName() + ", " + getFirstName();
    }


    /**
     * The method is not specified in the assignment text,
     * but is important to include in order to check equality for employees and patients.
     *
     * The method checks similarity: two Person objects are equal if their last name and social security number are identical
     * Identical similarity - two references to the same object
     * Content similarity - the content of two objects is compared by social security number and last name
     * @param o
     * @return returns true if similarity, false if inequality.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o){ //Identical similarity
            return true;
        }
        if (o instanceof Person){ //Content similarity
            Person tmp = (Person) o;
            return tmp.getSocialSecurityNumber().equals(this.socialSecurityNumber) &&
                    tmp.getLastName().equals(this.lastName);
        }
        return false;
    }


    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
