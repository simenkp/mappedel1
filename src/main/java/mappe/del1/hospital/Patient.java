package mappe.del1.hospital;

/**
 * A class that represents a patient
 * Inherits the interface Diagnosable, since a patient can only be diagnosed
 * Inherits the abstract class Person because a patient is a person
 */

public class Patient extends Person implements Diagnosable{
    private String diagnosis;

    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
        this.diagnosis = "";
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Overrided equals method from Person class, therefore calls for super
     * Checks equality between different Patient objects
     */
    @Override
    public boolean equals(Object o){
       return super.equals(o);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "diagnosis='" + diagnosis + "\n" + super.toString() +
                '}';
    }
}
