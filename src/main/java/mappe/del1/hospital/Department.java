package mappe.del1.hospital;
import mappe.del1.hospital.exception.RemoveException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * A class that represents a register of patients and employees for a department
 * Using Arraylist since I need a dynamic, ordered list.
 * It still would have been possible to use hashMap as you have unique social security numbers for every Person-object
 *
 * The class is an aggregate of Employee and Patient.
 * Multiplicity between Department, Employee and Patient is "one to many",
 * so both aggregation and composition would fit equally well.
 * Finally, I have chosen aggregation as I see a need to use the Patient and Employee objects independently of the Department registers.
 * Also, if the registers were to be deleted, I do not want the already registered Person objects to disappear with it.
 */


public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    public Department(String departmentName){
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * A method that add an employee to employees-list
     * Checks if employee already exists before adding
     * Changed return value to "boolean" instead of "void", thus I can preserve the value at registration in client
     *
     * @param employee the employee that is being added
     * @return return true if the registration is successful, false if not
     */
    public boolean addEmployee(Employee employee){
        if(!employees.contains(employee)){
            employees.add(employee);
            return true;
        }
        return false;
    }

    /**
     * A method that add a patient to patients-list
     * Checks if a patient already exists before adding
     * Changed the return value to "boolean" instead of "void", thus I can preserve the value at registration in client
     *
     * @param patient the patient that is being added
     * @return return true if the registration is successful, false if not
     */

    public boolean addPatient(Patient patient){
        if(!patients.contains(patient)){
            patients.add(patient);
            return true;
        }
        return true;
    }

    /**
     * A method that removes a Person object- either an Employee- or Patient object
     * Checks first if the lists "patients" and "employees" contains the Person we want to remove
     * uses instanceof to check if the Person object is an instance of Patient or Employee
     * @param person the person we want to remove
     * @return return true if the person is removed successfully
     * @throws NullPointerException if the person we want to remove is a null Object
     * @throws RemoveException if the person we want to remove is not registered
     */
    public boolean remove(Person person) throws RemoveException, NullPointerException {
        if(person == null){
            throw new NullPointerException("Person cannot be null");
        }
        if(!patients.contains(person) && !employees.contains(person)){
            throw new RemoveException( person.getFullName() + " is not registered");
        }
        else if(person instanceof Patient){
            Patient tmp = (Patient) person;
            patients.remove(tmp);
            return true;
        }
        else if(person instanceof Employee){
            Employee tmp = (Employee) person;
            employees.remove(tmp);
            return true;
        }
        return false;
    }

    /**
     * The method checks similarity: two Department objects are equal if their names are identical
     * Identical similarity - two references to the same object
     * Content similarity - the content of two objects is compared by department name
     * @param o
     * @return returns true if similarity, false if inequality.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o instanceof Department){
            Department tmp = (Department) o;
            return tmp.getDepartmentName().equals(this.departmentName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {
        String res = "departmentName: " + departmentName + "\n";
        for (Employee employee : employees){
            res += employee.toString() + "\n";
        }
        for(Patient patient : patients){
            res += patient.toString() + "\n";
        }
        return res;
    }
}
