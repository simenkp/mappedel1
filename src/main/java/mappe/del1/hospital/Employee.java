package mappe.del1.hospital;

import mappe.del1.hospital.Person;

/**
 * A class that represents an employee
 * Inherits from the abstract class Person
 */

public class Employee extends Person {
    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Override equals method from Person class, therefore calls for super
     * Checks equality between different Employee objects
     */

    @Override
    public boolean equals(Object o){
        return super.equals(o);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
